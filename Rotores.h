#ifndef ROTORES_H
#define ROTORES_H
#include "ListaElementos.h"
#include "LinkedList.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Rotores{
	public:
		Rotores();
		~Rotores();
		void cargaRotor();
		ListaElementos lista;
		LinkedList<char> rotor;

		
};

#endif
