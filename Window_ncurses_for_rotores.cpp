#include <ncurses.h>
#include "Rotores.h"
#include "ListaElementos.h"
#include "LinkedList.h"
#include <string.h>
using namespace std;
// To compile use: g++ -lncurses <filename>.cpp -o <endnamefile>

class Jackpot{
	public:
		Rotores* Rotor1 = new Rotores();
		Rotores* Rotor2 = new Rotores();
		Rotores* Rotor3 = new Rotores();
		Rotores* Rotor4 = new Rotores();
		Rotores* Rotor5 = new Rotores();
		
		Jackpot(){			
			Rotor1->cargaRotor();
			Rotor2->cargaRotor();
			Rotor3->cargaRotor();
			Rotor4->cargaRotor();
			Rotor5->cargaRotor();
		}
};


int main(){
	Jackpot* Juego;
	Juego = new Jackpot();
	char ElementoRotor1, ElementoRotor2, ElementoRotor3, ElementoRotor4, ElementoRotor5;	
	ElementoRotor1 = Juego->Rotor1->rotor.getcurr();
	ElementoRotor2 = Juego->Rotor2->rotor.getcurr();
	ElementoRotor3 = Juego->Rotor3->rotor.getcurr();
	ElementoRotor4 = Juego->Rotor4->rotor.getcurr();
	ElementoRotor5 = Juego->Rotor5->rotor.getcurr();
	


//Ncurses Window
	initscr();	
	int alto, ancho, start_y, start_x;
	alto = 25;
	ancho = 90;
	start_y = 2.5;
	start_x = 5;
	
	WINDOW * win = newwin(alto, ancho, start_y, start_x);
	refresh();	

	char border = '|';
	char topBottom = '-';
	char texto [1024];
	for(int i = 0; i<1024;i++) texto[i]^=texto[i];
	strcat(texto, ElementoRotor1);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor2);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor3);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor4);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor5);
	strcat(texto, "		 |");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "|    ");
	strcat(texto, ElementoRotor1);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor2);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor3);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor4);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor5);
	strcat(texto, "		 |");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "|    ");
	strcat(texto, ElementoRotor1);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor2);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor3);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor4);
	strcat(texto, " | ");
	strcat(texto, ElementoRotor5);



	box(win, (int)border, (int)topBottom);
	mvwprintw(win, 2.5, 5, texto);
	wrefresh(win);


	getch();
	endwin();

	return 0;
}
