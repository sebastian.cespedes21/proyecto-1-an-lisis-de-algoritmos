#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "Node.h"
template <typename E>

class LinkedList{
	public:
		Node<E>* head;
		Node<E>* curr;
		Node<E>* tail;
		int size;
		Node<E>* prev;
		LinkedList();
		E getcurr();
		void goToStart();
		void goToEnd();
		void goToPos(int nPos);
		int getPos();
		int getSize();
		void previous();
		void next();
		void appendN(Node<E>* pelement);
		void append(E pelement);
		void insert(E pelement);
		void remove();
		void insertHead(E pelement);
		void SearchPrev(Node<E>* pNodo);
		void circular();
		~LinkedList();
};

#endif // LINKEDLIST_H
