#include <ncurses.h>
#include "Rotores.h"
#include "ListaElementos.h"
#include "LinkedList.h"
#include <string.h>
using namespace std;

int main(){
//Ncurses Window
	initscr();	
	int alto, ancho, start_y, start_x;
	alto = 25;
	ancho = 90;
	start_y = 2.5;
	start_x = 5;
	
	WINDOW * win = newwin(alto, ancho, start_y, start_x);
	refresh();	

	char border = '|';
	char topBottom = '~';
	char texto [1024];
	for(int i = 0; i<1024;i++) texto[i]^=texto[i];	
	strcat(texto, "Elem rotor 1");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 2");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 3");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 4");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 5");
	strcat(texto, "		 |");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "|    ");
	strcat(texto, "Elem rotor 1");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 2");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 3");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 4");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 5");
	strcat(texto, "		 |");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "\n");
	strcat(texto, "|    ");
	strcat(texto, "Elem rotor 1");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 2");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 3");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 4");
	strcat(texto, " # ");
	strcat(texto, "Elem rotor 5");
	refresh();
	wrefresh(win);

	box(win, (int)border, (int)topBottom);
	mvwprintw(win, 2.5, 5, texto);
	wrefresh(win);


	getch();
	endwin();

	return 0;
}
